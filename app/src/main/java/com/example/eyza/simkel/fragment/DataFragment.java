package com.example.eyza.simkel.fragment;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.example.eyza.simkel.R;
import com.example.eyza.simkel.adapter.WargaAdapter;
import com.example.eyza.simkel.app.AppController;

import com.example.eyza.simkel.app.VolleyMultipartRequest;
import com.example.eyza.simkel.config.Config;
import com.example.eyza.simkel.database.AppDatabase;
import com.example.eyza.simkel.database.entity.Warga;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataFragment extends Fragment {

    static RecyclerView recyclerView;
    public static WargaAdapter wAdapter;
    public static List<Warga> wargaList = new ArrayList<>();
    public static AppDatabase db;
    private Gson gson;


    public DataFragment(){

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_data, container, false);
        ButterKnife.bind(this,view);



        gson = new Gson();

        Context context = getActivity();

        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view_warga);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(context,recyclerView,new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Warga warga = wargaList.get(position);
                //Gson gson = new Gson();

                //Toast.makeText(getActivity(),gson.toJson(warga),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }

        }));

        db = Room.databaseBuilder(getActivity(),
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        prepareWargaData();

        return view;
    }

    public static void prepareWargaData(){

        wargaList = db.WargaDao().getAllWarga();
        wAdapter = new WargaAdapter(wargaList);
        recyclerView.setAdapter(wAdapter);

    }

    @OnClick(R.id.btn_upload)
    public void upload(){
        if (wAdapter.ids.size() > 0){
            List<Warga> wargaList2;
            wargaList2 = db.WargaDao().getAllWargaByIds(wAdapter.ids);
            //Toast.makeText(getActivity(),gson.toJson(wargaList),Toast.LENGTH_SHORT).show();
            new UploadData(getActivity()).uploadToServer(wargaList2,true);



        } else {
            Toast.makeText(getActivity(),"Data Belum Dipilih",Toast.LENGTH_SHORT).show();
        }

    }

    @OnClick(R.id.btn_delete)
    public void delete(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());

        // set title
        alertDialogBuilder.setTitle("Pertanyaan");

        // set dialog message
        alertDialogBuilder
                .setMessage("Apakah Yakin Akan Menghapus Data ini?")
                .setCancelable(false)
                .setPositiveButton("Ya",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        if (wAdapter.ids.size() > 0){
                         db.WargaDao().deleteByIds(wAdapter.ids);
                         Toast.makeText(getActivity(),"DATA BERHASIL DIHAPUS",Toast.LENGTH_SHORT).show();
                         prepareWargaData();
                        } else {
                            Toast.makeText(getActivity(),"Data Belum Dipilih",Toast.LENGTH_SHORT).show();
                        }

                    }
                })
                .setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    public static class UploadData {
        private Context mContext;
        private ProgressDialog pd;

        public UploadData(Context context){
            this.mContext = context;
            pd = new ProgressDialog(context);
            pd.setMessage("Loading...");
        }



        public void uploadToServer(final List<Warga> wargaList, final boolean callback){


            pd.show();
            VolleyMultipartRequest mReq = new VolleyMultipartRequest(Request.Method.POST, Config.BASE_URL + "api/save_keluhan",
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            pd.dismiss();
                            AppController.getInstance().getRequestQueue().getCache().clear();

                            if (response.statusCode == 200){
                                String result = new String(response.data);

                                try {
                                    JSONObject json = new JSONObject(result);
                                    if (json.getString("result").equals("success")){
                                        Toast.makeText(mContext,"Data Berhasil Diupload",Toast.LENGTH_SHORT).show();
                                        //db.WargaDao().deleteById(warga.getId());

                                        AppDatabase DB = Room.databaseBuilder(mContext,
                                                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();

                                        List<Integer> ids = new ArrayList();

                                        for (Warga warga : wargaList){
                                            ids.add(warga.getId());
                                        }

                                        DB.WargaDao().deleteByIds(ids);

                                        if (callback == true){
                                            prepareWargaData();
                                        }

                                    } else {
                                        Toast.makeText(mContext,result,Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException e){
                                    e.printStackTrace();
                                }


                                Log.d("response :",new String(response.data));
                            } else {
                                Toast.makeText(mContext,"ERROR",Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    pd.dismiss();

                    if(error.networkResponse.data!=null) {
                        String body;
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");
                            Toast.makeText(mContext,body,Toast.LENGTH_SHORT).show();
                            Log.d("Error :",body);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }){
                @Override
                protected Map<String,String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    Gson gson = new Gson();
                    params.put("data",gson.toJson(wargaList));
                    return params;
                }

                @Override
                protected Map<String, DataPart> getByteData() throws AuthFailureError {
                    Map<String, DataPart> params = new HashMap<>();

                    for (Warga warga : wargaList){
                        long imagename = System.currentTimeMillis();
                        if (warga.getVideo() != null){
                            params.put("video["+warga.getId()+"]", new DataPart("VID_"+String.valueOf(imagename)+".mp4",convertVideoToBytes(warga.getVideo())));
                        }

                        if (warga.getPhoto1() != null){
                            params.put("photo1["+warga.getId()+"]", new DataPart("PIC_1_"+String.valueOf(imagename)+".jpg",convertImageToBytes(warga.getPhoto1())));
                        }

                        if (warga.getPhoto2() != null){
                            params.put("photo2["+warga.getId()+"]", new DataPart("PIC_2_"+String.valueOf(imagename)+".jpg",convertImageToBytes(warga.getPhoto2())));
                        }

                        if (warga.getPhoto3() != null){
                            params.put("photo3["+warga.getId()+"]", new DataPart("PIC_3_"+String.valueOf(imagename)+".jpg",convertImageToBytes(warga.getPhoto3())));
                        }
                        if (warga.getPhoto4() != null){
                            params.put("photo4["+warga.getId()+"]", new DataPart("PIC_4_"+String.valueOf(imagename)+".jpg",convertImageToBytes(warga.getPhoto4())));
                        }
                        if (warga.getPhoto5() != null){
                            params.put("photo5["+warga.getId()+"]", new DataPart("PIC_5_"+String.valueOf(imagename)+".jpg",convertImageToBytes(warga.getPhoto5())));
                        }
                    }

                    return params;
                }
            };

            mReq.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


            AppController.getInstance().addToRequestQueue(mReq);
        }

        public  byte[] convertImageToBytes(String path) {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            return stream.toByteArray();
        }

        public  byte[] convertVideoToBytes(String path) {
            byte[] videoBytes = null;
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                FileInputStream fis = new FileInputStream(new File(path));
                byte[] buf = new byte[1024];
                int n;
                while (-1 != (n = fis.read(buf)))
                    baos.write(buf, 0, n);

                videoBytes = baos.toByteArray();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return videoBytes;
        }
    }

}
