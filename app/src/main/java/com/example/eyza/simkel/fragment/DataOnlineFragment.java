package com.example.eyza.simkel.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.eyza.simkel.R;
import com.example.eyza.simkel.config.Config;
import com.example.eyza.simkel.config.Session;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DataOnlineFragment extends Fragment {

    @BindView(R.id.webview) WebView webView;
    private ProgressDialog pd;
    public DataOnlineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_data_online, container, false);

        ButterKnife.bind(this,v);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebChromeClient(new WebChromeClient(){
            @Override
            public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
                //Required functionality here
                return super.onJsAlert(view, url, message, result);
            }
        });
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                return false;
            }

            @Override
            public void onPageFinished(WebView eview,String url){
                pd.dismiss();
            }
        });
        webView.addJavascriptInterface(new WebInterface(getActivity()),"Android");
        webView.loadUrl(Config.BASE_URL+"api/data_keluhan/"+ String.valueOf(Session.getUserId(getActivity()))+"/"+Session.getRole(getActivity()));

        pd = new ProgressDialog(getActivity());
        pd.setMessage("Loading...");
        pd.show();

        return v;
    }

    public boolean canGoBack() {
        return webView.canGoBack();
    }

    public void goBack() {
        webView.goBack();
    }

    public class WebInterface {
        Context mContext;

        public WebInterface(Context context){
            this.mContext = context;
        }

        @JavascriptInterface
        public String getNativeVar(){
            Map<String, String> data = new HashMap<String, String>();
            data.put("user_id",String.valueOf(Session.getUserId(getActivity())));
            data.put("role",Session.getRole(getActivity()));
            Gson gson = new Gson();
            return gson.toJson(data);
        }
    }

}
