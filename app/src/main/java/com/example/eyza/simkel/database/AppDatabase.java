package com.example.eyza.simkel.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.example.eyza.simkel.database.dao.WargaDao;
import com.example.eyza.simkel.database.entity.Warga;

@Database(entities = {Warga.class},version = 5,exportSchema = false)
@TypeConverters({DateTypeConverter.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract WargaDao WargaDao();
}
