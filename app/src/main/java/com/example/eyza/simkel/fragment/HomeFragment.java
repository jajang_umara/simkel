package com.example.eyza.simkel.fragment;


import android.Manifest;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.location.Location;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;

import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;

import android.support.v4.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.VideoView;




import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;

import com.example.eyza.simkel.R;

import com.example.eyza.simkel.config.MyLocation;
import com.example.eyza.simkel.config.Session;
import com.example.eyza.simkel.database.AppDatabase;
import com.example.eyza.simkel.database.entity.Warga;

import com.iceteck.silicompressorr.SiliCompressor;



import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.FileNotFoundException;

import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import java.util.List;
import java.util.Locale;



import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

import static com.basgeekball.awesomevalidation.ValidationStyle.BASIC;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    @BindView(R.id.tanggal_kunjungan) EditText tanggal;
    @BindView(R.id.select_agama) Spinner agama;
    @BindView(R.id.select_status) Spinner status;
    @BindView(R.id.nama_warga) EditText nama;
    @BindView(R.id.nik_warga) EditText nik;
    @BindView(R.id.kk_warga) EditText kk;
    @BindView(R.id.jumlah_keluarga) EditText jumlahKeluarga;
    @BindView(R.id.check_jk_pria) RadioButton jkPria;
    @BindView(R.id.check_jk_wanita) RadioButton jkWanita;
    @BindView(R.id.alamat_warga) EditText alamat;
    @BindView(R.id.keluhan_warga) EditText keluhan;
    @BindView(R.id.preview_1) ImageView image1;
    @BindView(R.id.preview_2) ImageView image2;
    @BindView(R.id.preview_3) ImageView image3;
    @BindView(R.id.preview_4) ImageView image4;
    @BindView(R.id.preview_5) ImageView image5;
    @BindView(R.id.preview_6) VideoView video1;
    @BindView(R.id.fab) FloatingActionButton fab;
    @BindView(R.id.take_picture_1) Button btn1;
    @BindView(R.id.take_picture_2) Button btn2;
    @BindView(R.id.take_picture_3) Button btn3;
    @BindView(R.id.take_picture_4) Button btn4;
    @BindView(R.id.take_picture_5) Button btn5;

    private Double longitude,latitude;
    private AppDatabase db;
    private AwesomeValidation awesomeValidation;
    MyLocation myLocation = new MyLocation();
    private Uri file1,file2,file3,file4,file5,video;
    private String fileString1,fileString2,fileString3,fileString4,fileString5;
    public static String videoCompressed;
    private ProgressDialog pd;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this,view);



        tanggal.setText(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault()).format(new Date()));




        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.agama_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        agama.setAdapter(adapter);

        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(getActivity(),
                R.array.status_array, android.R.layout.simple_spinner_item);

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        status.setAdapter(adapter2);

        db = Room.databaseBuilder(getActivity(),
                AppDatabase.class, "MyDB").allowMainThreadQueries().fallbackToDestructiveMigration().build();

        video1.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Log.d("video", "setOnErrorListener ");
                return true;
            }
        });

        return view;
    }

    @Override
    public void onActivityCreated(Bundle bundle){
        super.onActivityCreated(bundle);
        awesomeValidation = new AwesomeValidation(BASIC);
        awesomeValidation.addValidation(getActivity(),R.id.nik_warga,RegexTemplate.NOT_EMPTY,R.string.nameerror);
        awesomeValidation.addValidation(getActivity(),R.id.kk_warga,RegexTemplate.NOT_EMPTY,R.string.nameerror);
        awesomeValidation.addValidation(getActivity(),R.id.nama_warga,RegexTemplate.NOT_EMPTY,R.string.nameerror);
        awesomeValidation.addValidation(getActivity(),R.id.jumlah_keluarga,RegexTemplate.NOT_EMPTY,R.string.nameerror);
        awesomeValidation.addValidation(getActivity(),R.id.alamat_warga,RegexTemplate.NOT_EMPTY,R.string.nameerror);
        awesomeValidation.addValidation(getActivity(),R.id.keluhan_warga, RegexTemplate.NOT_EMPTY,R.string.nameerror);

    }

    public MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {

        @Override
        public void gotLocation(Location location) {
            // TODO Auto-generated method stub
            longitude = location.getLongitude();
            latitude = location.getLatitude();
            Toast.makeText(getActivity(),"Titik Gps sudah Ditemukan",Toast.LENGTH_SHORT).show();



        }
    };

    @OnClick(R.id.btn_simpan_offline)
    public void saveOffline(){
        saveData("offline");
    }

    @OnClick(R.id.btn_simpan_online)
    public void saveOnline(){
        saveData("online");
    }

    public void saveData(String mode) {
        if(awesomeValidation.validate()){
            if (longitude == null || latitude == null){
                Toast.makeText(getActivity(),"LOKASI GPS MASIH KOSONG",Toast.LENGTH_SHORT).show();
            } else {
                Warga warga = new Warga();
                warga.setNama(nama.getText().toString());

                DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                Date tanggalNew;
                try {
                    tanggalNew = formatter.parse(tanggal.getText().toString());
                    warga.setTanggal(tanggalNew);
                } catch (ParseException pe){
                    pe.printStackTrace();
                }

                warga.setNik(nik.getText().toString());
                warga.setNomorKK(kk.getText().toString());
                warga.setJumlahKeluarga(Integer.parseInt(jumlahKeluarga.getText().toString()));
                warga.setAgama(agama.getSelectedItem().toString());
                warga.setStatusPernikahan(status.getSelectedItem().toString());
                warga.setAlamat(alamat.getText().toString());
                warga.setKeluhan(keluhan.getText().toString());

                if (jkPria.isChecked()){
                    warga.setJenisKelamin("pria");
                }
                if (jkWanita.isChecked()){
                    warga.setJenisKelamin("wanita");
                }

                warga.setLatitude(latitude);
                warga.setLongitude(longitude);
                warga.setPhoto1(fileString1);
                warga.setPhoto2(fileString2);
                warga.setPhoto3(fileString3);
                warga.setPhoto4(fileString4);
                warga.setPhoto5(fileString5);
                warga.setVideo(videoCompressed);
                warga.setUserId(Session.getUserId(getActivity()));
                if (mode == "offline"){
                    db.WargaDao().insertWarga(warga);
                    Toast.makeText(getActivity(),"DATA BERHASIL DISIMPAN",Toast.LENGTH_SHORT).show();
                } else {
                    List<Warga> daftarWarga = new ArrayList<>();
                    daftarWarga.add(warga);
                    new DataFragment.UploadData(getActivity()).uploadToServer(daftarWarga,false);
                }

                latitude = null;
                longitude = null;

                clearForm();


            }
        }
    }

    public void clearForm(){
        tanggal.setText(new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault()).format(new Date()));
        nama.setText("");
        nik.setText("");
        kk.setText("");
        jumlahKeluarga.setText("");
        alamat.setText("");
        keluhan.setText("");
        image1.setImageResource(android.R.color.transparent);
        image2.setImageResource(android.R.color.transparent);
        image3.setImageResource(android.R.color.transparent);
        image4.setImageResource(android.R.color.transparent);
        image5.setImageResource(android.R.color.transparent);
        video1.setVideoPath("");
        btn1.setText("Ambil Photo");
        btn2.setText("Ambil Photo");
        btn3.setText("Ambil Photo");
        btn4.setText("Ambil Photo");
        btn5.setText("Ambil Photo");
    }







    @OnClick(R.id.take_picture_1)
    public void takePicture1(){
        takePicture(1,false);
    }

    @OnClick(R.id.take_picture_2)
    public void takePicture2(){
        takePicture(2,false);
    }
    @OnClick(R.id.take_picture_3)
    public void takePicture3(){
        takePicture(3,false);
    }
    @OnClick(R.id.take_picture_4)
    public void takePicture4(){
        takePicture(4,false);
    }
    @OnClick(R.id.take_picture_5)
    public void takePicture5(){
        takePicture(5,false);
    }

    @OnClick(R.id.take_picture_6)
    public void takePicture6(){
        takePicture(6,true);
    }

    @OnClick(R.id.play_video)
    public void playVideo(){
        if (videoCompressed != null)
            video1.start();


    }

    @OnClick(R.id.stop_video)
    public void stopVideo(){
        if (videoCompressed != null)
            video1.stopPlayback();


    }

    @OnClick(R.id.fab)
    public void clickFab(){
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            myLocation.getLocation(getActivity(), locationResult);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    101);
        }

    }

    public void takePicture(int recCode,boolean video1)
    {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                        PackageManager.PERMISSION_GRANTED) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            Intent i;
            if (video1){
                i = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);


                    i.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                    video = Uri.fromFile(getOutputMediaFile(0));
                    i.putExtra(MediaStore.EXTRA_OUTPUT, video);


            } else {
                i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            }
            if (recCode == 1){
                file1 = Uri.fromFile(getOutputMediaFile(1));
                i.putExtra(MediaStore.EXTRA_OUTPUT, file1);
            } else if (recCode == 2){
                file2 = Uri.fromFile(getOutputMediaFile(1));
                i.putExtra(MediaStore.EXTRA_OUTPUT, file2);
            } else if (recCode == 3){
                file3 = Uri.fromFile(getOutputMediaFile(1));
                i.putExtra(MediaStore.EXTRA_OUTPUT, file3);
            } else if (recCode == 4){
                file4 = Uri.fromFile(getOutputMediaFile(1));
                i.putExtra(MediaStore.EXTRA_OUTPUT, file4);
            } else if (recCode == 5){
                file5 = Uri.fromFile(getOutputMediaFile(1));
                i.putExtra(MediaStore.EXTRA_OUTPUT, file5);
            }

            i.putExtra("android.intent.extras.CAMERA_FACING", Camera.CameraInfo.CAMERA_FACING_BACK);
            i.putExtra("android.intent.extras.LENS_FACING_FRONT", Camera.CameraInfo.CAMERA_FACING_BACK);
            i.putExtra("android.intent.extra.USE_BACK_CAMERA", true);

            startActivityForResult(i,recCode);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[] {
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE },
                    102);
        }

    }



    public static File getOutputMediaFile(int type) {

        // External sdcard location
        File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "E-LAPOR DATA");

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        // Preparing media file naming convention
        // adds timestamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");
        } else if (type ==0) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        switch(requestCode){
            case 1:
                if(resultCode == RESULT_OK){
                    fileString1 = compressImage(file1);
                    image1.setImageURI(Uri.parse(fileString1));
                    image1.requestFocus();
                    btn1.setText("Ganti Photo");
                }
                break;
            case 2:
                if(resultCode == RESULT_OK){
                    fileString2 = compressImage(file2);
                    image2.setImageURI(Uri.parse(fileString2));
                    image2.requestFocus();
                    btn2.setText("Ganti Photo");
                }
                break;
            case 3:
                if(resultCode == RESULT_OK){
                    fileString3 = compressImage(file3);
                    image3.setImageURI(Uri.parse(fileString3));
                    image3.requestFocus();
                    btn3.setText("Ganti Photo");
                }
                break;
            case 4:
                if(resultCode == RESULT_OK){
                    fileString4 = compressImage(file4);
                    image4.setImageURI(Uri.parse(fileString4));
                    image4.requestFocus();
                    btn4.setText("Ganti Photo");
                }
                break;
            case 5:
                if(resultCode == RESULT_OK){
                    fileString5 = compressImage(file5);
                    image5.setImageURI(Uri.parse(fileString5));
                    image5.requestFocus();
                    btn5.setText("Ganti Photo");
                }
                break;
            case 6:
                if(resultCode == RESULT_OK){


                    File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getActivity().getPackageName() + "/media/videos");
                    if (f.mkdirs() || f.isDirectory())
                        //compress and output new video specs
                        new VideoCompressAsyncTask(getActivity()).execute(data.getData().getPath(), f.getPath());

                    /*
                    if (videoCompressed != null){
                        //video1.setVideoURI(Uri.parse(videoCompressed));
                        video1.setVideoPath(videoCompressed);
                    }
                    Toast.makeText(getActivity(),videoCompressed,Toast.LENGTH_SHORT).show();
                    */
                }
                break;

        }
    }

    public String compressImage(Uri fileData){
        File f = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + getActivity().getPackageName() + "/media/image");
        if (f.mkdirs() || f.isDirectory()){
            return SiliCompressor.with(getActivity()).compress(fileData.getPath(),f,true);

        }
        return null;
    }

    public void getCompressedVideo(String compressed){
        this.videoCompressed = compressed;
    }

    public  Bitmap decodeUri(Context c, Uri uri, final int requiredSize) {

            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            try {
                BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o);
            } catch (FileNotFoundException e){
                e.printStackTrace();
            }
            int width_tmp = o.outWidth, height_tmp = o.outHeight;
            int scale = 1;

            while (true) {
                if (width_tmp / 2 < requiredSize || height_tmp / 2 < requiredSize)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            Bitmap result = null;
            try {
                result = BitmapFactory.decodeStream(c.getContentResolver().openInputStream(uri), null, o2);
            } catch (FileNotFoundException ef){
                ef.printStackTrace();
            }
            return result;
    }

    class VideoCompressAsyncTask extends AsyncTask<String, String, String> {

        Context mContext;
        String sourceFile;
        ProgressDialog pd;

        public VideoCompressAsyncTask(Context context){
            mContext = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage("Proses Kompresi Video...");
            pd.show();

        }

        @Override
        protected String doInBackground(String... paths) {
            String filePath = null;
            sourceFile = paths[0];
            try {

                filePath = SiliCompressor.with(mContext).compressVideo(paths[0], paths[1]);


            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            return  filePath;

        }


        @Override
        protected void onPostExecute(String compressedFilePath) {
            super.onPostExecute(compressedFilePath);
            File fileDeleted = new File(sourceFile);
            fileDeleted.delete();
            video1.setVideoPath(compressedFilePath);
            video1.requestFocus();
            pd.dismiss();
            getCompressedVideo(compressedFilePath);
            Log.i("Silicompressor", "Path: "+compressedFilePath);
        }
    }



}
