package com.example.eyza.simkel.database.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.eyza.simkel.database.entity.Warga;

import java.util.List;


@Dao
public interface WargaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertWarga(Warga warga);

    @Query("SELECT * FROM tb_warga")
    List<Warga> getAllWarga();

    @Query("SELECT * FROM tb_warga WHERE id IN(:ids)")
    List<Warga> getAllWargaByIds(List<Integer> ids);

    @Query("SELECT * FROM tb_warga WHERE id = :id")
    Warga getWargaById(int id);

    @Query("DELETE FROM tb_warga WHERE id IN(:ids)")
    void deleteByIds(List<Integer> ids);

    @Delete
    void deleteData(Warga warga);

}
