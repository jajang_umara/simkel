package com.example.eyza.simkel.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by OVAY on 1/4/2019.
 */

public class Session {

    static final String KEY_ID = "kelurahan_id";
    static final String LOGIN_STATUS = "login_status";
    static final String USER_ID = "user_id";
    static final String ROLE = "role";

    public static SharedPreferences getSharedPreference(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setKeyId(Context context, int id){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(KEY_ID, id);
        editor.apply();
    }

    public static int getKeyId(Context context){
        return getSharedPreference(context).getInt(KEY_ID,0);
    }

    public static boolean getLoginStatus(Context context){
        return getSharedPreference(context).getBoolean(LOGIN_STATUS,false);
    }

    public static void setLoginStatus(Context context, boolean status){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putBoolean(LOGIN_STATUS, status);
        editor.apply();
    }

    public static int getUserId(Context context){
        return getSharedPreference(context).getInt(USER_ID,0);
    }

    public static void setUserId(Context context, int id){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putInt(USER_ID, id);
        editor.apply();
    }

    public static String getRole(Context context){
        return getSharedPreference(context).getString(ROLE,"");
    }

    public static void setRole(Context context, String role){
        SharedPreferences.Editor editor = getSharedPreference(context).edit();
        editor.putString(ROLE, role);
        editor.apply();
    }
}
