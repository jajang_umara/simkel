package com.example.eyza.simkel.database.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "tb_setting")
public class Setting {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;

    @ColumnInfo(name="nama_kelurahan")
    @SerializedName("nama_desa")
    private String namaKelurahan;

    @ColumnInfo(name="area")
    private String areaKelurahan;

    @ColumnInfo(name="id_kelurahan")
    @SerializedName("id_kelurahan")
    private int idKelurahan;

    public void setId(@NonNull int id) {
        this.id = id;
    }

    @NonNull
    public int getId() {
        return id;
    }

    public int getIdKelurahan() {
        return idKelurahan;
    }

    public void setIdKelurahan(int idKelurahan) {
        this.idKelurahan = idKelurahan;
    }

    public String getAreaKelurahan() {
        return areaKelurahan;
    }

    public void setAreaKelurahan(String areaKelurahan) {
        this.areaKelurahan = areaKelurahan;
    }
}
