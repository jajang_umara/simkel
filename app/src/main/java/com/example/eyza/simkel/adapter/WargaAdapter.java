package com.example.eyza.simkel.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


import com.example.eyza.simkel.database.entity.Warga;


import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.example.eyza.simkel.R;

public class WargaAdapter extends RecyclerView.Adapter<WargaAdapter.MyViewHolder> {
    public static List<Warga> wargaList;
    public static List<Integer> ids = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.id_nik_warga) TextView nik;
        @BindView(R.id.id_nama_warga) TextView nama;
        @BindView(R.id.id_alamat_warga) TextView alamat;
        @BindView(R.id.id_tanggal_warga) TextView tanggal;
        @BindView(R.id.row_check) CheckBox rowCheck;

        public MyViewHolder(View view){
            super(view);
            ButterKnife.bind(this,view);

        }
    }

    public WargaAdapter(List<Warga> wargaList){
        this.wargaList = wargaList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.warga_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        Warga warga = wargaList.get(position);
        holder.nik.setText("NIK : "+warga.getNik());
        holder.nama.setText(warga.getNama());
        holder.alamat.setText("Alamat : "+warga.getAlamat());
        holder.tanggal.setText("Tanggal : "+new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault()).format(warga.getTanggal()));
        holder.rowCheck.setTag(position);
        holder.rowCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox cb = (CheckBox) view;
                Integer pos = (Integer) holder.rowCheck.getTag();

                if (cb.isChecked()) {
                    wargaList.get(pos).setSelected(true);
                    ids.add(wargaList.get(pos).getId());
                } else {
                    wargaList.get(pos).setSelected(false);
                    ids.remove(Integer.valueOf(wargaList.get(pos).getId()));
                }



            }

        });
    }

    @Override
    public int getItemCount() {
        return wargaList.size();
    }
}
