package com.example.eyza.simkel;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.example.eyza.simkel.app.AppController;
import com.example.eyza.simkel.config.Config;
import com.example.eyza.simkel.config.Session;
import com.example.eyza.simkel.database.AppDatabase;
import com.example.eyza.simkel.database.entity.Setting;
import com.example.eyza.simkel.database.entity.Warga;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.android.PolyUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {
    private ProgressDialog progressDialog;
    private AwesomeValidation awesomeValidation;
    private AppDatabase db;

    @BindView(R.id.username) EditText username;
    @BindView(R.id.password) EditText password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");

        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(LoginActivity.this,R.id.username, "[a-zA-Z0-9\\\\s]+",R.string.nameerror);
        awesomeValidation.addValidation(LoginActivity.this,R.id.password, "[a-zA-Z0-9\\\\s]+",R.string.nameerror);

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "MyDB").allowMainThreadQueries().build();
    }

    @OnClick(R.id.btn_login)
    public void login()
    {

        if (awesomeValidation.validate()) {

            progressDialog.show();
            StringRequest strReq = new StringRequest(Request.Method.POST, Config.BASE_URL + "api/login", new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    JSONObject jsonObject = null;
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    try {
                        jsonObject = new JSONObject(response);

                        if (jsonObject.getString("result").equals("success")) {
                            Setting sett = gson.fromJson(response,Setting.class);
                            Session.setLoginStatus(LoginActivity.this,true);
                            Session.setUserId(LoginActivity.this,sett.getId());
                            Session.setKeyId(LoginActivity.this,sett.getIdKelurahan());
                            Session.setRole(LoginActivity.this,jsonObject.getString("role"));
                            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                            startActivity(intent);
                            finish();

                        } else {
                            Toast.makeText(getApplicationContext(), "Nama User atau Password Salah", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    progressDialog.hide();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.hide();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("username", username.getText().toString());
                    params.put("password", password.getText().toString());

                    return params;
                }

            };

            AppController.getInstance().addToRequestQueue(strReq, "Str Req");


        }
    }
}
